setup:
	# install tools

install_terraform: 
	# install terraform infrastructure as code provisioning tool
	if [ -n "$(which terraform | grep "No" )" ]; then \
		wget https://releases.hashicorp.com/terraform/0.13.0/terraform_0.13.0_linux_amd64.zip \
		&& unzip terraform_0.13.0_linux_amd64.zip \
		&& sudo mv terraform /usr/local/bin/ \
		&& terraform -v ; \
	else \
		terraform -v; \
	fi;

install_ansible:
	if [ -z "$(which python | grep "No" )" ]; then \
		python -m pip install --user ansible; \
	elif [ -z "$(which python3 | grep "No" )" ]; then \
		python3 -m pip install --user ansible; \
	else \
		sudo apt install -y python3 \
		&& python3 -m pip install --user ansible; \
		# exit 1; \
	fi;

all: setup install_terraform install_ansible