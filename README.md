[![Sinatra cats CI/CD](https://github.com/Lorioux/sinatra/actions/workflows/main.yml/badge.svg)](https://github.com/Lorioux/sinatra/actions/workflows/main.yml)

### Sinatra Cats App CI/CD  
Configuration of Continuous Delivery of very simple Sinatra application with a random startup time. Here we will use sinatra cats repository (`submodule` cats).
We Set up an infrastructure with automated zero-downtime deployment with AWS + Terraform, following the description:
1. Deploy the version 1.0.0 of the “https://github.com/Streetbees/cats” application on a fault-tolerant, automated infrastructure;
1. Automate the deployment of the version 2.0.1 of the same application without any downtime;
1. You may add any features to the application that might help you with this task, as long as it does not change its existing functionalities and behaviours;
1. Make sure that every step is documented so that any developer on your team could manage to use your automation;

### The infrastructure architecture
----
<img src="./assets/sinatra.svg">

<br/>

### Continuous Delivery Configuration
----
The continuous integration and continuous deployment processes are defined as following. There is a standard (base) application in production, hence version 1.0.0, so new feature are being developed incrementally. Every push on child branch triggers build, test and analysis jobs. 
Every pull requests trigger code revision for aproval, and release created and published trigger a complete build and release and deployment of the app into public cloud AWS, as per Terraform (IaC) definitions, Only for the main branch.

<img src="./assets/ci.svg">

<br/>

### Running the App Containers
----
You may set these variables through the process's ENV:

- `RACK_ENV`: Defaults to `production`.
- `PORT`: Defaults to `8000`.
- `WEB_CONCURRENCY`: Number of processes managed by [Puma](http://puma.io/).
Defaults to `1`.
- `MAX_THREADS`: Number of threads per process. Defaults to `1`.

#### Running it

If you're bundling the gems, use `bundle exec puma`; otherwise, `puma` is enough.
