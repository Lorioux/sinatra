terraform {
  required_providers {
      aws = {
          source = "hashicorp/aws"
          version = "~> 3.0"
      }
  }

  backend "s3" {
    bucket = "sinatra-indie-state"
    key    = "terraform-state/sinatra"
    region = "us-east-1"
  }

}

provider "aws" {
  region = "us-east-1"
  profile = "terraform"
}

module "vpc" {
    source = "terraform-aws-modules/vpc/aws" 


    name = "SinatraVPC"
    cidr = "176.0.0.0/16"

    azs = [ "us-east-1a", "us-east-1b", "us-east-1c" ]
    enable_dns_hostnames = true 
    enable_dns_support = true
    
    public_subnets = [
        "176.0.1.0/24",
        "176.0.2.0/24",
        "176.0.3.0/24"
    ]

    tags = {
        Project = "Sinatra"
    }

}

module "security-alb" {

    source = "terraform-aws-modules/security-group/aws"

    depends_on = [
      module.vpc
    ]

    name = "alb-listeners-sg"

    vpc_id = module.vpc.vpc_id 
    ingress_rules = ["http-80-tcp"]
    ingress_cidr_blocks = ["0.0.0.0/0"]

    # tags  = {
    #     Project = "Sinatra"
    # }
  
}


module "ecs" {
    source = "terraform-aws-modules/ecs/aws"

    create_ecs = true

    name = "SinatraCluster"

    container_insights = false
    
    capacity_providers = ["FARGATE_SPOT", "FARGATE"]

    default_capacity_provider_strategy = [
        {
            capacity_provider = "FARGATE_SPOT"
        }
    ]

    tags = {
        Project = "Sinatra"
    }

}

module "alb" {
  source = "terraform-aws-modules/alb/aws"
  version = "~> 6.0"

  depends_on = [
    module.vpc
  ]

  name = "sinatra-ecs-alb"

  load_balancer_type = "application"

  vpc_id = module.vpc.vpc_id 
  subnets = module.vpc.public_subnets
  security_groups = [module.security-alb.security_group_id]

  target_groups = [
    {
        name_prefix = "ecs"
        backend_protocol = "HTTP"
        backend_port = 80
        target_type = "ip"
        target_group_attributes = [{
            "deregistration_delay.timeout_seconds" = 60
        }]
    }
  ]

  http_tcp_listeners = [
  {
    port = 80
    protocol = "HTTP"
    target_group_index = 0    
  }]

  tags = {
    Project = "Sintra"
  }

}

# resource "aws_kms_key" "kms" {
#   key_usage = "ENCRYPT_DECRYPT"
#   tags = {
#         Project = "Sinatra"
#     }
# }

module "service" {
  
    source = "trussworks/ecs-service/aws"

    depends_on = [
      module.alb,
    #   aws_kms_key.kms,
      module.security-alb
    ]

    name = "Sinatra"

    ecs_cluster = {
        arn = module.ecs.ecs_cluster_arn
        name = module.ecs.ecs_cluster_name
    }

    ecs_use_fargate = true 

    target_container_name = "Sinatra-prod"

    container_definitions = jsonencode([{
            name = "Sinatra-prod"
            image = "devopsxpro/sinatra:1.0.0"
            cpu = 10
            essential = true
            portMappings = [
                {
                    containerPort : 80
                    hostPort: 80
                }
            ]
        }]
    )

    # should not be mandatory
    # kms_key_id = aws_kms_key.kms.arn

    ecs_vpc_id = module.vpc.vpc_id

    assign_public_ip = true

    ecs_subnet_ids = module.vpc.public_subnets 

    associate_alb = true

    alb_security_group = module.security-alb.security_group_id

    lb_target_groups = [
        {
            container_port = 80
            container_health_check_port = 80
            lb_target_group_arn = module.alb.target_group_arns[0]
        }
    ]
    
    health_check_grace_period_seconds = 60
    tasks_desired_count = 2
    environment = "prod"
    # tags = {
    #     Project = "Sinatra"
    # }
}

resource "aws_route53_zone" "zones" {
  name = "sinatracats.io"

    vpc {
        vpc_id =  module.vpc.vpc_id
        vpc_region = "us-east-1"
    }
    comment = "ECS ELB custom dns name"
  tags = {
        Project = "Sinatra"
    }
}

data "aws_route53_zone" "zone" {
    # name = keys(aws_route53_zone.zone.name)[0]
    zone_id = "${aws_route53_zone.zones.zone_id}"
  depends_on = [
    aws_route53_zone.zones
  ]
  tags = {
    Project = "Sinatra"
  }
}

resource "aws_route53_record" "alb-alias" {
    depends_on = [
        data.aws_route53_zone.zone
    ]

    # zone_name = data.aws_route53_zone.zone.name
    zone_id = data.aws_route53_zone.zone.zone_id

    name = "app"

    type = "A"
    # ttl = 60

    alias {
        name = module.alb.lb_dns_name
        zone_id = module.alb.lb_zone_id
        evaluate_target_health = false
    }
}

output "endpoint" {
  value = "http://${aws_route53_record.alb-alias.fqdn}"
}