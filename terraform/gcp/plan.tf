terraform {
  required_providers {
      google = {
          source = "hashicorp/google"
      }
  }
}

provider "google" {
  region = "us-central1"
  project = "ccoe-solutions"
}


data "google_iam_policy" "admin" {
  binding {
    role = "roles/owner"
    members = [
      "user:magidjm@gmail.com",
    ]
  }
}

resource "google_compute_network" "demonwk" {
  name                    = "demo-net"
  auto_create_subnetworks = false

}

resource "google_compute_subnetwork" "subnetwk" {
  name          = "demo-subnetwork"
  
  ip_cidr_range = "10.0.10.0/28"
  region        = "us-central1"
  network       = google_compute_network.demonwk.id
  secondary_ip_range {
    range_name    = "tf-test-secondary-range-update1"
    ip_cidr_range = "192.168.10.0/24"
  }
}

# resource "google_compute_subnetwork" "consubnetwk" {
#   name          = "convpc-subnetwork"
  
#   ip_cidr_range = "10.0.20.0/28"
#   region        = "us-central1"
#   network       = google_compute_network.demonwk.id
#   secondary_ip_range {
#     range_name    = "tf-test-secondary-range-update1"
#     ip_cidr_range = "192.168.10.0/24"
#   }
# }

resource "google_vpc_access_connector" "connector" {
    provider = google-beta
    region = "us-central1"
  project = "ccoe-solutions"
  name          = "demo-con-vpc"
    subnet {
        name = google_compute_subnetwork.subnetwk.name
    }

    machine_type = "f1-micro"

    depends_on = [
      google_compute_subnetwork.subnetwk
    ]
}


resource "google_compute_firewall" "demofwl" {
  name    = "demo-firewall"
  network = google_compute_network.demonwk.name
  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["80", "8080", "1000-2000"]
  }

  allow {
    protocol = "tcp"
    ports = ["443", "22"]
  }

  source_tags = ["web"]
}


