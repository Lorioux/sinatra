data "aws_iam_user" "userone" {
  user_name = "terraform"
}

data "aws_iam_user" "usertwo" {
  user_name = "devops"
}

data "aws_vpc" "vpc" {
  tags = {
    Project = var.project_name
    Name    = "${var.project_name}-vpc"
  }

  depends_on = [
    module.vpc
  ]
}

data "aws_subnet_ids" "subnet_ids" {
  vpc_id = data.aws_vpc.vpc.id
}


module "eks" {
  source = "terraform-aws-modules/eks/aws"
  cluster_version = "1.21"

  # create_eks = data.aws_eks_cluster.eks.id != " " ? true : false

  cluster_name    = var.cluster_name
  vpc_id          = data.aws_vpc.vpc.id
  subnets         = data.aws_subnet_ids.subnet_ids.ids

  
  cluster_enabled_log_types = [ ] #"api", "authenticator"

  # worker_security_group_id = module.security_group_worker_mngt.security_group_id

  # node_groups = [{
  #   instance_types = ["t2.micro"]
  #   capacity_type = "SPOT"
  #   create_lauch_template = true 
  #   desired_capacity = 2
  #   max_capacity = 4
  #   min_size = 2
  #   eni_delete = true
  #   source_security_group_ids = []
  #   name = "NODE_GROUP"
  #   kubelet_extra_args = "--node-labels=node.kubernetes.io/lifecycle=spot"
  # }]

  worker_groups_launch_template = [
    {
      name                          = "NODE_SPOT"
      instance_types                = ["t3.small"]
      override_instance_types       = ["t3.small"]
      asg_max_size                  = 4
      asg_desired_capacity          = 2
      capacity_type                 = "SPOT"
      subnet_ids                    = module.vpc.public_subnets
      additional_security_group_ids = [module.security_group_node.security_group_id]
      
      update_config = {
        max_unavailable : 1
      }

      kubelet_extra_args = "--node-labels=node.kubernetes.io/lifecycle=spot"
      
      public_ip          = true

      tag = {
        key                 = "Project"
        value               = var.project_name
        propagate_at_launch = true
      }
    }
  ]

  create_fargate_pod_execution_role = true
  fargate_subnets                   = module.vpc.private_subnets
  # Fargate profiles for control-plane and worker pods creations
  fargate_profiles = {

    default = {
      name = "default_profile"
      selectors = [
        {
          namespace = "kube-system"
          labels = {
            k8s-app = "kube-dns"
          }
        },
        {
          namespace = "default"
          labels = {
            app = "sinatra-app"
          }
        }
      ]

      tags = {
        Project = var.project_name
      }
    }
  }


  # Where to write the kubernetes config
  kubeconfig_output_path = var.KUBE_HOME
  write_kubeconfig       = true
  kubeconfig_file_permission = 0777
  kubeconfig_aws_authenticator_command = "aws"

  kubeconfig_aws_authenticator_command_args = [
      "--region",
      "${var.region}",
      "eks",
      "get-token",
      "--cluster-name",
      "${var.cluster_name}",
      # "--role",
      # "${module.eks.cluster_iam_role_arn}",
  ]

  kubeconfig_aws_authenticator_env_variables = {
    AWS_PROFILE = var.profile
  }

  # AWS AUTH (Kubernet labels)
  map_users = [
    {
      userarn  = data.aws_iam_user.userone.arn 
      username = "terraform"
      groups   = ["system:masters"]
    },
    {
      userarn  = data.aws_iam_user.usertwo.arn 
      username = "devops"
      groups   = ["system:masters"]
    },
  ]

  tags = {
    Project = var.project_name
  }
}