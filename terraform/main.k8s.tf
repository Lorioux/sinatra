
# retrieve the cluster
data "aws_eks_cluster" "cluster" {
  depends_on = [
    module.eks
  ]
  name = var.cluster_name
}


data "aws_eks_cluster_auth" "auth" {
  name = module.eks.cluster_id
}

# create OIDC Provider and associate with the AWS ELB Controler role
resource "aws_iam_openid_connect_provider" "oidc_provider" {
  url             = module.eks.cluster_oidc_issuer_url
  thumbprint_list = [var.cluster_oidc_thumbprint_value]
  client_id_list  = ["sts.amazonaws.com"]
  tags = {
    Project = var.project_name
  }
}

# create aws load balancer controller policy
resource "aws_iam_policy" "aws_alb_controller_policy" {
  name   = "AWSLoadBalancerControllerIAMPolicy"
  policy = file("./policies/aws_alb_controller_policy.json")
}

# create aws lb controller role
resource "aws_iam_role" "aws_lb_controller_role" {

  name = "AmazonEKSLoadBalancerControllerRole"

  description = "AWS Load Balancer Controller Role"
  assume_role_policy = jsonencode({
    Version : "2012-10-17",
    Statement : [
      {
        "Effect" : "Allow",
        "Principal" : {
          "Federated" : aws_iam_openid_connect_provider.oidc_provider.arn
        },
        "Action" : "sts:AssumeRoleWithWebIdentity",
        "Condition" : {
          "StringEquals" : {
            "${aws_iam_openid_connect_provider.oidc_provider.arn}:aud" : var.aws_elb_controler_trust_value
          }
        }
      }
    ]
  })

  tags = {
    Project = var.project_name
  }

}

# associate role with policy
resource "aws_iam_role_policy_attachment" "controller_role_policy" {
  role       = aws_iam_role.aws_lb_controller_role.name
  policy_arn = aws_iam_policy.aws_alb_controller_policy.arn
}

############
# KUBERNETES CONFIGURATION
############

provider "kubernetes" {

  config_path            = var.KUBE_HOME
  host                   = module.eks.cluster_endpoint   # data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(module.eks.cluster_certificate_authority_data) # data.aws_eks_cluster.cluster.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.auth.token

  exec {
    api_version = "client.authentication.k8s.io/v1alpha1"
    args        = ["eks", "get-token", "--cluster-name", var.cluster_name]
    command     = "aws"
  }
}

# create the service account on your cluster
resource "kubernetes_service_account" "aws_elb_controller_sa" {
  metadata {
    labels = {
      "app.kubernetes.io/component" : "controller"
      "app.kubernetes.io/name" : "aws-load-balancer-controller"
    }

    name      = "aws-load-balancer-controller"
    namespace = "kube-system"
    annotations = {
      "eks.amazonaws.com/role-arn" : aws_iam_role.aws_lb_controller_role.arn
    }
  }
}

# horizontal pods autoscaling using autoscaling/v1
resource "kubernetes_horizontal_pod_autoscaler" "sinatra-hpa" {
  metadata {
    name = "sinatra-app-hpa"
  }

  spec {
    max_replicas                      = 10
    min_replicas                      = 2
    target_cpu_utilization_percentage = 80
    scale_target_ref {
      kind = "Deployment"
      name = "sinatra-deployment"
    }
  }
}


#####
# HELM 
#####

provider "helm" {
  kubernetes {
    config_path            = var.KUBE_HOME
    host                   = module.eks.cluster_endpoint   # data.aws_eks_cluster.cluster.endpoint
    cluster_ca_certificate = base64decode(module.eks.cluster_certificate_authority_data) # data.aws_eks_cluster.cluster.certificate_authority[0].data)
    token                  = data.aws_eks_cluster_auth.auth.token

    exec {
        api_version = "client.authentication.k8s.io/v1alpha1"
        args        = ["eks", "get-token", "--cluster-name", var.cluster_name]
        command     = "aws"
    }
  }
}

# install the TargetGroupBinding CRDs

module "aws_load_balancer_controler" {
  source  = "terraform-module/release/helm"
  version = "2.6.0"

  repository = "https://aws.github.io/eks-charts"
  namespace  = "kube-system"

  app = {
    name          = "aws-load-balancer-controller"
    version       = "1.2.7"
    chart         = "aws-load-balancer-controller"
    force_update  = true
    wait          = false
    recreate_pods = false
    deploy        = 1
    recreate_pods = 1
    cleanup_on_fail = 1
  }

  values = [file("./k8s/values.yml")]

  set = [
    {
      name  = "clusterName"
      value = var.cluster_name
    },
    {
      name  = "serviceAccount.create"
      value = false
    },
    {
      name  = "serviceAccount.name"
      value = "aws-load-balancer-controller"
    },
    {
        name = "ingressClass"
        value = "alb"
    },
    {
        name = "vpcId"
        value = module.vpc.vpc_id
    },

    {
        name = "region"
        value = var.region
    }
  ]

}


