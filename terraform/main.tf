terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }

  # provision a S3 backend to store infrastructure state
  backend "s3" {
    bucket = "sinatra-indie-state"
    key    = "terraform-state/sinatra"
    region = "us-east-1"
  }
}

provider "aws" {
  region  = var.region
  profile = var.profile
}



module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 3.0.0"

  name = "${var.project_name}-vpc"

  cidr = var.vpc_cidrblock

  azs = [var.availability_zones.zone-a, var.availability_zones.zone-b]

  private_subnets = [var.private_subnet_1_cidrblock, var.private_subnet_2_cidrblock]

  public_subnets = [var.public_subnet_1_cidrblock, var.public_subnet_2_cidrblock]

  enable_nat_gateway   = true
  enable_dns_hostnames = true
  enable_dhcp_options  = true

  vpc_tags = {
    Project = var.project_name
    Name    = "${var.project_name}-vpc"
  }

  private_subnet_tags = {
    Project                           = var.project_name
    Name                              = "${var.project_name}-pvtsubnet"
    "kubernetes.io/role/internal-elb" = 1
  }
  public_subnet_tags = {
    Project                  = var.project_name
    Name                     = "${var.project_name}-pubsubnet"
    "kubernetes.io/role/elb" = 1
  }

}

# a securty group for cluster worker nodes
module "security_group_node" {

  source      = "terraform-aws-modules/security-group/aws"
  version     = "4.3.0"
  description = "Allow TLS and non-TLS inbound traffics"

  name   = var.security_group_name
  vpc_id = module.vpc.vpc_id

  ingress_cidr_blocks = [var.vpc_cidrblock]
  egress_cidr_blocks  = ["0.0.0.0/0"]
  ingress_rules       = ["https-443-tcp", "http-80-tcp"]

  tags = {
    Name    = var.security_group_name
    Project = var.project_name

    # special tag required for aws alb
    # "kubernetes.io/cluster/${var.cluster_name}" = "shared"
  }
}

# securitiy group for cluster worker nodes management
# module "security_group_worker_mngt" {

#   source      = "terraform-aws-modules/security-group/aws"
#   version     = "4.3.0"
#   description = "Allow SSH inbound traffics"
#   name        = "sinatra-wg-MNGT"
#   vpc_id      = module.vpc.vpc_id

#   ingress_rules       = ["ssh-tcp"]
#   ingress_cidr_blocks = [var.vpc_cidrblock]

#   tags = {
#     Project = var.project_name

#     # # special tag required for aws alb
#     # "kubernetes.io/cluster/${var.cluster_name}" = "shared"
#   }
# }