output "vpc_id" {
  description = "VPC ID"
  value       = module.vpc.vpc_id
}

output "private_subnet_ids" {
  description = "Private subnet IDs"
  value       = module.vpc.private_subnets
}

output "public_subnent_ids" {
  description = "Public subnet IDs"
  value       = module.vpc.public_subnets
}



# # output "private_subnet_1" {
#   description = "Private subnet 1 ID"
#   value       = aws_subnet.private_subnet_1.id
# }

# output "private_subnet_2" {
#   description = "Private subnet 2 ID"
#   value       = aws_subnet.private_subnet_2.id
# }

# output "public_subnet_1" {
#   value        = aws_subnet.public_subnet_1.id
#   description = "Public subnet 1 ID"
# }

# output "public_subnet_2" {
#   value        = aws_subnet.public_subnet_1.id
#   description = "Public subnet 2 ID"
# }


# output "security_group_node" {
#   description = "Cluster Nodes Security group ID"
#   value       = module.security_group_node.security_group_id
# }

# output "sinatra_domain" {
#   description = "Route 53 host zone"
#   value       = aws_route53_zone.sinatra_domain.zone_id
#   sensitive   = true
# }


output "eks_endpoint" {
  description = "EKS cluster public endpoint access dns name"
  value       = "https://${module.eks.cluster_endpoint}"
  sensitive   = true
}

output "kubeconfig-certificate-authority-data" {
  description = "EKS cluster certificate data"
  value       = module.eks.cluster_certificate_authority_data
  sensitive   = true
}

output "eks_cluster_oidc_url" {
  description = "EKS Cluster OIDC"
  value       = module.eks.cluster_oidc_issuer_url
  sensitive   = true
}

output "aws-elb-controler-role" {
  description = "AWS LB Controller role ARN TO use in Kubernetes Service Account"
  value       = aws_iam_role.aws_lb_controller_role.arn
  sensitive   = true
}

