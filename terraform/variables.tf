variable "project_name" {
  description = "Project stack name"
  type        = string
  default     = "sinatra"
}

variable "profile" {
  description = "Project deployment profile"
  type        = string
  default     = "terraform"
}

variable "region" {
  description = "Project deployment region"
  type        = string
  default     = "us-east-1"
}

variable "KUBE_HOME" {
  type = string 
  description = "Kubeconfig path set the environment as TF_VAR_KUBE_HOME"
}

variable "vpc_cidrblock" {
  type        = string
  default     = "172.0.0.0/16"
  description = "VPC CIDR Bloclk at us-east-1 region"
}

variable "availability_zones" {
  type        = map(string)
  description = "Availability zones map"
  default = {
    zone-a = "us-east-1a"
    zone-b = "us-east-1b"
  }
}

variable "private_subnet_1_cidrblock" {
  type        = string
  default     = "172.0.0.0/24"
  description = "CIDR block for private subnet 1 zone A "
}

variable "private_subnet_2_cidrblock" {
  type        = string
  default     = "172.0.1.0/24"
  description = "CIDR block for private subnet 2 zone B "
}

variable "public_subnet_1_cidrblock" {
  type        = string
  default     = "172.0.3.0/24"
  description = "CIDR block for public subnet 1 zone A "
}

variable "public_subnet_2_cidrblock" {
  type        = string
  default     = "172.0.4.0/24"
  description = "CIDR block for public subnet 2 zone B "
}


variable "security_group_name" {
  type        = string
  default     = "sinatra_security_group"
  description = "Security groups name for Cluster EC2 instances into private subnets"
}

variable "cluster_name" {
  type        = string
  description = "EKS Cluster Name"
  default     = "sinatra"
}

variable "cluster_oidc_thumbprint_value" {
  type        = string
  default     = "9e99a48a9960b14926bb7f3b02e22da2b0ab7280"
  description = "Default thumbprint of Root CA for EKS OIDC, Valid until 2037 - TO create OIDC Provider"
}
variable "aws_elb_controler_trust_value" {
  type        = string
  description = "AWS Load Balancer role's Trust relationship value"
  default     = "system:serviceaccount:kube-system:aws-load-balancer-controller"
}
